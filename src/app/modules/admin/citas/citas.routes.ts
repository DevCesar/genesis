import { Routes } from '@angular/router';
import { CitasComponent } from './citas.component';


export default [
    {
        path: '',
        component: CitasComponent,
    },
] as Routes;
