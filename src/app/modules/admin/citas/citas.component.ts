import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

@Component({
    selector: 'app-citas',
    standalone: true,
    templateUrl: './citas.component.html',
    styleUrl: './citas.component.scss',
    encapsulation: ViewEncapsulation.None,
    imports: [ReactiveFormsModule, MatFormFieldModule, MatDatepickerModule,
        MatNativeDateModule]
})
export class CitasComponent implements OnInit {

    citaForm: FormGroup;

    constructor(private fb: FormBuilder) { }

    ngOnInit(): void {
        this.citaForm = this.fb.group({
            nombre: ['', Validators.required],
            fecha: ['', Validators.required],
            hora: ['', Validators.required]
        });
    }

    generarURL() {
        const nombre = this.citaForm.get('nombre').value;
        const fecha = this.citaForm.get('fecha').value;
        const hora = this.citaForm.get('hora').value;

        // Aquí generas la URL de conexión a Jitsi utilizando los datos del formulario
        const urlJitsi = `https://meet.jit.si/${nombre}-${fecha}-${hora}`;

        // Aquí puedes redirigir al usuario a la URL de Jitsi
        console.log(urlJitsi);
    }
}
